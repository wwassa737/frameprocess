import numpy as np
from matplotlib import pylab as plt
import wave

np.set_printoptions(threshold=1000)
np.seterr('ignore')


def segment(signal, W, SP, Window):
    '''
    W　:窓のサイズ
    SP　:overlapping
    Window　:窓関数
    '''
    L = len(signal)
    SP = int(W * SP)
    # SP = int(W * SP)
    N = int((L - W) / SP + 1)
    # N = int((L - W) / SP + 1)
    Window = Window.flatten(1)
    Index = (np.tile(np.arange(1, W + 1), (N, 1)) + np.tile(np.arange(0, N) * SP, (W, 1)).T).T
    # Index=[[    1   257   513 ... 76545 76801 77057]
    #      [    2   258   514 ... 76546 76802 77058]
    #      [    3   259   515 ... 76547 76803 77059]
    #      ...
    #      [  510   766  1022 ... 77054 77310 77566]
    #      [  511   767  1023 ... 77055 77311 77567]
    #      [  512   768  1024 ... 77056 77312 77568]]
    print(Index)
    hw = np.tile(Window, (N, 1)).T
    Seg = signal[Index] * hw
    return Seg


def OverlapAdd2(XNEW, yphase, windowLen, ShiftLen):
    FrameNum = XNEW.shape[1]
    Spec = XNEW * np.exp(1j * yphase)
    ShiftLen = int(ShiftLen)
    if windowLen % 2:
        Spec = np.concatenate((Spec, np.flipud(np.conj(Spec[1:, ]))))
    else:
        Spec = np.concatenate((Spec, np.flipud(np.conj(Spec[1:-1, :]))))
    sig = np.zeros(((FrameNum - 1) * ShiftLen + windowLen, 1))
    for i in range(FrameNum):
        start = i * ShiftLen
        spec = Spec[:, [i]]
        sig[start:start + windowLen] = sig[start:start + windowLen] + np.real(np.fft.ifft(spec, axis=0))
    return sig


if __name__ == '__main__':
    W = 1024  # window length
    SP = 0.5  # overlapping

    #     input_file = "sample_bnoise_0.2"
    input_file = r"C:\Users\Ken\Desktop\audio_lab\frameprocess\sample"
    # 　ファイル読み込み　or 書き込み
    wr = wave.open(input_file + ".wav", "r")

    out_file = (input_file + "_out")
    ww = wave.open(out_file + ".wav", "w")
    if ww is None:
        print("Output file has already in use")
        exit(1)
    if wr is None:
        print("Iutput file has already in use")
        exit(1)

    fs = wr.getframerate()
    print("fs:", fs)

    num_frames = wr.getnframes()
    chunk_size = int(np.fix(60 * fs))
    saved_params = None
    print("nframes:", wr.getnframes())
    frames_read = 0
    p = (1, 2, fs, wr.getnframes(), 'NONE', 'not compressed')
    # ww.setnchannels(1)
    # ww.setsampwidth(2)
    # ww.setframerate(fs)
    ww.setparams(p)

    print("フレーム数", wr.getnframes())

    while (frames_read < num_frames):
        frames = num_frames - frames_read if frames_read + chunk_size > num_frames else chunk_size
        signal = wr.readframes(frames)
        signal = np.frombuffer(signal, dtype="int16") / 0x8000

        print("signal", signal, "\n")

        # オーバーラップ+窓かけ
        # wnd=np.hamming(W)
        wnd = np.hanning(W)
        # print("wnd:",wnd)
        # wnd = 1 / 2 * np.ones(W)# ``再合成可能

        y = segment(signal=signal, W=W, SP=SP, Window=wnd)

        # fft処理
        Y = np.fft.fft(y, axis=0)
        YPhase = np.angle(Y[0:int(len(Y) / 2) + 1, :])

        # 振幅スペクトル
        Y = np.abs(Y[0:int(len(Y) / 2) + 1, :])

        # ===フレーム処理=======================================================

        X = Y

        # ===フレーム処理ここまで===============================================
        # 窓かけ+逆fft
        output = OverlapAdd2(X, YPhase, W, SP * W)

        frames_read = frames_read + frames
        output = np.array(output.reshape(-1) * 0x8000, dtype=np.int16)
        ww.writeframes(output.reshape(-1))
        # ww.write_frames(output)

    wr.close()
    ww.close()
